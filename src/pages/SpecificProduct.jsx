import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function SpecificProduct() {
	const { user } = useContext(UserContext);

	const history = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const [count, setCount] = useState(0);

	const purchase = (productId) => {
		fetch('http://localhost:4000/products/checkout', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
			},
			body: JSON.stringify({
				productId: productId,
				userId: user.id,
			}),
		}).then((data) => {
			if (data) {
				Swal.fire({
					title: 'Product added to cart!',
					icon: 'success',
				});
				history('/products/active');
			} else {
				Swal.fire({
					title: 'Product was not added to cart',
					icon: 'danger',
				});
			}
		});
	};

	const add = () => {
		setCount(count + 1);
	};

	const subtract = () => {
		setCount(count - 1);
	};

	useEffect(() => {
		fetch(`http://localhost:4000/products/${productId}`)
			.then((res) => res.json())
			.then((data) => {
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
			});
	}, [productId]);

	return (
		<Container>
			<Card>
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>Price: Php {price}</h6>
				</Card.Body>
				<Card.Footer>
					{user.id !== null ? (
						<>
							<Card.Text>Amount: {count} </Card.Text>
							<Button variant="dark" onClick={() => subtract(productId)}>
								-
							</Button>
							<Button variant="dark" onClick={() => add(productId)}>
								+
							</Button>
							<br></br>
							<Button variant="danger" onClick={() => purchase(productId)}>
								Purchase
							</Button>
						</>
					) : (
						<Link className="btn btn-danger btn-block" to="/login">
							Log in to Purchase
						</Link>
					)}
				</Card.Footer>
			</Card>
		</Container>
	);
}
