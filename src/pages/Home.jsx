import React, { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
	const data = {
		title: 'WELCOME!',
		content: 'Sports Apparell for you!',
		destination: '/products/active',
		label: 'Buy now!',
	};

	return (
		<Fragment>
			<Banner data={data} />
			<Highlights />
		</Fragment>
	);
}
