import React, { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
	const { user } = useContext(UserContext);
	const history = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNum, setMobileNum] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email: email,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					Swal.fire({
						title: 'Duplicate email found',
						icon: 'error',
						text: 'Kindly provide another email to complete the registration.',
					});
				} else {
					fetch('http://localhost:4000/users/register', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNum: mobileNum,
							password: password1,
						}),
					})
						.then((res) => res.json())
						.then((data) => {
							if (data === true) {
								setFirstName('');
								setLastName('');
								setEmail('');
								setMobileNum('');
								setPassword1('');
								setPassword2('');

								Swal.fire({
									title: 'Registration successful',
									icon: 'success',
									text: 'Welcome to OS!',
								});

								history('/login');
							} else {
								Swal.fire({
									title: 'Something wrong',
									icon: 'error',
									text: 'Please try again.',
								});
							}
						});
				}
			});
		setFirstName('');
		setLastName('');
		setEmail('');
		setMobileNum('');
		setPassword1('');
		setPassword2('');
	}

	useEffect(() => {
		if (
			firstName !== '' &&
			lastName !== '' &&
			email !== '' &&
			mobileNum.length === 11 &&
			password1 !== '' &&
			password2 !== '' &&
			password1 === password2
		) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, firstName, lastName, mobileNum.length, password1, password2]);

	return user.id !== null ? (
		<Navigate to="/products/active" />
	) : (
		<Form
			onSubmit={(e) => registerUser(e)}
			className="bg-secondary text-center p-4 w-50 m-3"
		>
			<h1 className="text-light">Register</h1>
			<Form.Group controlId="firstName" className="text-light">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					className="d-flex"
					placeholder="Enter first name"
					value={firstName}
					onChange={(e) => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName" className="text-light">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter last name"
					value={lastName}
					onChange={(e) => setLastName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="text-light" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo" className="text-light">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Mobile Number"
					value={mobileNum}
					onChange={(e) => setMobileNum(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="text-light" controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={(e) => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-2 text-light" controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={(e) => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>
			{isActive ? (
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
			) : (
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			)}
		</Form>
	);
}
