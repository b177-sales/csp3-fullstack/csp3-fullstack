import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { Link, useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function LoginForm(props) {
	const history = useNavigate();
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);
	const [willRedirect, setWillRedirect] = useState(false);

	const authenticate = (e) => {
		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (typeof data.access !== 'undefined') {
					localStorage.setItem('token', data.access);
					retrieveUserDetails(data.access);
					Swal.fire({
						title: 'Login Successful',
						icon: 'success',
						text: 'Welcome!',
					});
				} else {
					Swal.fire({
						title: 'Authentication failed',
						icon: 'error',
						text: 'Check your login details and try again.',
					});
				}
			});
	};

	const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${token}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);
				setUser({
					id: data._id,
					isAdmin: data.isAdmin,
				});
				if (data.isAdmin === true) {
					setWillRedirect(true);
				} else {
					if (props.location.state.from === 'cart') {
						history.goBack();
					} else {
						setWillRedirect(true);
					}
				}
			});
	};

	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);

	return (function() {
		if (user.id !== null && user.isAdmin === false) {
			return <Navigate to="/products/active" />;
		} else if (willRedirect === true && user.isAdmin === true) {
			return <Navigate to="/adminView" />;
		} else {
			return (
				<Row className="w-75">
					<Col xs="12" md="6">
						<Card className="text-center bg-secondary p-2">
							<h2 className="text-center m-2 text-light">Log In</h2>
							<Form onSubmit={(e) => authenticate(e)}>
								<Card.Body>
									<Form.Group controlId="userEmail" className="text-light">
										<Form.Label>Email:</Form.Label>
										<Form.Control
											type="email"
											placeholder="Enter your email"
											value={email}
											onChange={(e) => setEmail(e.target.value)}
											required
										/>
									</Form.Group>

									<Form.Group controlId="password" className="mt-1 text-light">
										<Form.Label>Password:</Form.Label>
										<Form.Control
											type="password"
											placeholder="Enter your password"
											value={password}
											onChange={(e) => setPassword(e.target.value)}
											required
										/>
									</Form.Group>

									{isActive ? (
										<Button
											variant="danger"
											type="submit"
											id="submitBtn"
											className="m-2"
										>
											Submit
										</Button>
									) : (
										<Button
											variant="dark"
											type="submit"
											id="submitBtn"
											disabled
										>
											Submit
										</Button>
									)}
									<p className="text-center text-dark">
										Don't have an account yet?{' '}
										<Link to="/register">Click here</Link> to register.
									</p>
								</Card.Body>
							</Form>
						</Card>
					</Col>
				</Row>
			);
		}
	})();
}
