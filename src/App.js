import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import SpecificProduct from './pages/SpecificProduct';
import Products from './pages/Products';
import Error from './pages/Error';
import Home from './pages/Home';
import Logout from './pages/Logout';
import Register from './pages/Register';
import AdminView from './components/AdminView';
import Login from './pages/Login';
import './App.css';
import { UserProvider } from './UserContext';
import Footer from './components/Footer';
import Cart from './components/Cart';
import UserOrderHistory from './components/UserOrderHistory';
import UserProfile from './components/UserProfile';

function App() {
	const [user, setUser] = useState({
		id: null,
		isAdmin: null,
	});

	const unsetUser = () => {
		localStorage.clear();
	};

	useEffect(() => {
		fetch('http://localhost:4000/users/details', {
			headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
		})
			.then((res) => res.json())
			.then((data) => {
				if (typeof data._id !== 'undefined') {
					setUser({ id: data._id, isAdmin: data.isAdmin });
				} else {
					setUser({ id: null, isAdmin: null });
				}
			});
	}, []);

	return (
		<UserProvider value={{ user, setUser, unsetUser }}>
			<Router>
				<AppNavbar />
				<Container>
					<Routes>
						<Route path="/" element={<Home />} />
						<Route path="/products/active" element={<Products />} />
						<Route path="/userprofile" element={<UserProfile />} />
						<Route path="/products/:productId" element={<SpecificProduct />} />
						<Route path="/adminview" element={<AdminView />} />
						<Route path="/userorderhistory" element={<UserOrderHistory />} />
						<Route path="/register" element={<Register />} />
						<Route path="/login" element={<Login />} />
						<Route path="/logout" element={<Logout />} />
						<Route path="/cart" element={<Cart />} />
						<Route path="*" element={<Error />} />
						<Route path="/cart" element={<Cart />} />
					</Routes>
				</Container>
				<Footer />
			</Router>
		</UserProvider>
	);
}

export default App;
