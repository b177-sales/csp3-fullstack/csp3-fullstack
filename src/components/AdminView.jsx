import React, { useEffect, useState } from 'react';
import { Form, Table, Button, Modal, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Swal from 'sweetalert2';

export default function AdminView() {
	//Hold all products
	const [products, setProducts] = useState([]);

	// useState to add and edit products (Similar to Register)
	const [id, setId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	// Used for Modal(Pop-Up)
	const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	const [toggle, setToggle] = useState(false);

	const [ordersList, setOrdersList] = useState([]);

	// Used for Modal Add and Edit Product
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	// Open Edit
	const openEdit = (productId) => {
		fetch(`http://localhost:4000/products/${productId}`)
			.then((res) => res.json())
			.then((data) => {
				setId(productId);
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
			});

		setShowEdit(true);
	};

	const closeEdit = () => {
		setName('');
		setDescription('');
		setPrice(0);
		setShowEdit(false);
	};
	// End of Edit

	const productsData = () => {
		fetch('http://localhost:4000/products', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				setProducts(data);
			});
	};

	useEffect(() => {
		productsData();
	}, []);

	// Add Product
	const addProduct = (e) => {
		e.preventDefault();

		fetch('http://localhost:4000/products', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					Swal.fire({
						title: 'Item successfully added!',
						icon: 'success',
					});
					setName('');
					setDescription('');
					setPrice(0);
					productsData();
					closeAdd();
				} else {
					alert('Something went wrong.');
					closeAdd();
				}
			});
	};
	// End of Add Product

	//Update Product
	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`http://localhost:4000/products/${productId}`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					Swal.fire({
						title: 'Item successfully edited!',
						icon: 'success',
					});
					setName('');
					setDescription('');
					setPrice(0);
					productsData();
					closeEdit();
				} else {
					Swal.fire({
						title: 'Something went wrong',
						icon: 'error',
					});
					closeEdit();
				}
			});
	};
	// End of Update

	const activateProduct = (productId) => {
		fetch(`http://localhost:4000/products/${productId}/activate`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					productsData();
					Swal.fire({
						title: 'Item successfully added to active products!',
						icon: 'success',
					});
				} else {
					Swal.fire({
						title: 'Something went wrong',
						icon: 'error',
					});
				}
			});
	};

	const archiveProduct = (productId) => {
		fetch(`http://localhost:4000/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					productsData();
					Swal.fire({
						title: 'Item successfully archived!',
						icon: 'success',
					});
				} else {
					Swal.fire({
						title: 'Something went wrong',
						icon: 'error',
					});
				}
			});
	};

	const deleteProduct = (productId) => {
		fetch(`http://localhost:4000/products/${productId}/delete`, {
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					productsData();
					Swal.fire({
						title: 'Item removed from inventory!',
						icon: 'success',
					});
				} else {
					Swal.fire({
						title: 'Something went wrong',
						icon: 'error',
					});
				}
			});
	};

	useEffect(() => {
		fetch('http://localhost:4000/users/orders', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				setOrdersList(data);
			});
	}, []);

	const toggler = () => {
		if (toggle === true) {
			setToggle(false);
		} else {
			setToggle(true);
		}
	};

	return (
		<React.Fragment>
			<div className="text-center my-4 text-secondary">
				<h2 className="bg-light">Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button className="mr-1" variant="primary" onClick={openAdd}>
						Add New Product
					</Button>
					{toggle === false ? (
						<Button variant="success" onClick={() => toggler()}>
							Show User Orders
						</Button>
					) : (
						<Button variant="danger" onClick={() => toggler()}>
							Show Product Details
						</Button>
					)}
				</div>
			</div>
			{toggle === false ? (
				<Table striped bordered hover responsive>
					<thead className="bg-secondary text-white">
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>Availability</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody className="bg-light text-dark">
						{products.map((product) => {
							return (
								<tr key={product._id}>
									<td>
										<Link to={`/products/${product._id}`}>{product.name}</Link>
									</td>
									<td>{product.description}</td>
									<td>{product.price}</td>
									<td>
										{product.isActive ? (
											<span>Available</span>
										) : (
											<span>Unavailable</span>
										)}
									</td>
									<td>
										<Button
											variant="primary"
											size="sm"
											onClick={() => openEdit(product._id)}
										>
											Update
										</Button>
										<Button
											variant="secondary"
											size="sm"
											onClick={() => deleteProduct(product._id)}
										>
											Delete
										</Button>
										{product.isActive ? (
											<Button
												variant="danger"
												size="sm"
												onClick={() => archiveProduct(product._id)}
											>
												Disable
											</Button>
										) : (
											<Button
												variant="success"
												size="sm"
												onClick={() => activateProduct(product._id)}
											>
												Enable
											</Button>
										)}
									</td>
								</tr>
							);
						})}
					</tbody>
				</Table>
			) : (
				<Card>
					<Card.Header className="bg-secondary text-white">
						{' '}
						All Orders
					</Card.Header>
					{ordersList.map((order) => {
						return (
							<Card.Body key={order._id}>
								<div>
									<h6>Product {order.productId}</h6>
									<p>
										Purchased on{' '}
										{moment(order.purchasedOn).format('MM-DD-YYYY')}
									</p>
									<p>Purchased by {order.userId}</p>
									<h6>
										Total:{' '}
										<span className="text-warning">₱ {order.totalAmount}</span>
									</h6>
									<hr />
								</div>
							</Card.Body>
						);
					})}
				</Card>
			)}

			{/* Add Product */}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={(e) => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add New Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name:</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter product name"
								value={name}
								onChange={(e) => setName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description:</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter product description"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price:</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={(e) => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>
							Close
						</Button>
						<Button variant="success" type="submit">
							Submit
						</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			{/* End of Add Product */}

			{/* Edit Product */}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={(e) => editProduct(e, id)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name:</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter product name"
								value={name}
								onChange={(e) => setName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description:</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter product description"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price:</Form.Label>
							<Form.Control
								type="number"
								placeholder="Enter product price"
								value={price}
								onChange={(e) => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>
							Close
						</Button>
						<Button variant="success" type="submit">
							Submit
						</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			{/* End of Add Product */}
		</React.Fragment>
	);
}
