import React from 'react';
import { Card, Container, Row } from 'react-bootstrap';
import '../App.css';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function ProductCard({ productProp }) {
	const { _id, name, description, price } = productProp;

	return (
		<Container className="m-1" id="productCard">
			<Row>
				<Card
					style={{ width: '18rem', height: '23rem' }}
					bg="secondary"
					className="m-1"
				>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>
						<Link className="btn btn-dark" to={`/products/${_id}`}>
							View Item
						</Link>
					</Card.Body>
				</Card>
			</Row>
		</Container>
	);
}

ProductCard.propTypes = {
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
	}),
};
