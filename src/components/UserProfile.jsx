import React, { useState, useEffect } from 'react';
import { Container, Card } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

export default function SpecificProduct() {
	const { userId } = useParams();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNum, setMobileNum] = useState('');
	const [email, setEmail] = useState('');

	useEffect(() => {
		fetch(`http://localhost:4000/users/details`)
			.then((res) => res.json())
			.then((data) => {
				setFirstName(data.firstName);
				setLastName(data.lastName);
				setMobileNum(data.mobileNum);
				setEmail(data.email);
			});
	}, [userId]);

	return (
		<Container>
			<Card>
				<Card.Header>
					<h4>My Profile</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>Last Name: {lastName}</Card.Text>
					<Card.Text>First Name: {firstName}</Card.Text>
					<Card.Text>Mobile Number: {mobileNum}</Card.Text>
					<Card.Text>Email: {email}</Card.Text>
				</Card.Body>
			</Card>
		</Container>
	);
}
