import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row className="px-2">
			<Col xs={12} md={3}>
				<Card style={{ width: '16rem' }} className="cardHighlight">
					<Card.Img
						style={{ height: 150 }}
						src="https://res.cloudinary.com/dtxy6jvmo/image/upload/v1657629094/e-commerce/1000px-Baseball-Header_uipkxq.webp"
					/>
				</Card>
			</Col>
			<Col xs={12} md={3}>
				<Card style={{ width: '16rem' }} className="cardHighlight">
					<Card.Img
						style={{ height: 150 }}
						src="https://res.cloudinary.com/dtxy6jvmo/image/upload/v1657630215/e-commerce/My_project_oaaue3.jpg"
					/>
				</Card>
			</Col>
			<Col xs={12} md={3}>
				<Card style={{ width: '16rem' }} className="cardHighlight">
					<Card.Img
						style={{ height: 150 }}
						src="https://res.cloudinary.com/dtxy6jvmo/image/upload/v1657629252/e-commerce/science-baseball-cricket-bats-01-gty-llr-210522_1621699849823_hpMain_16x9_1600_ahwid1.jpg"
					/>
				</Card>
			</Col>
			<Col xs={12} md={3}>
				<Card style={{ width: '16rem' }} className="cardHighlight">
					<Card.Img
						style={{ height: 150 }}
						src="https://res.cloudinary.com/dtxy6jvmo/image/upload/v1657630498/e-commerce/pexels-pixabay-163408_whbllp.jpg"
					/>
				</Card>
			</Col>
		</Row>
	);
}
