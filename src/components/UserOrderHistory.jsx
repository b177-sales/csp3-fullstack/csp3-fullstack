import React, { useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import moment from 'moment';

export default function UserOrderHistory() {
	const [ordersList, setOrdersList] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/users/myOrders', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				setOrdersList(data);
			});
	}, []);

	return (
		<React.Fragment>
			<div className="text-center my-4 text-secondary">
				<h2 className="bg-light">User Order History</h2>
			</div>
			<Card>
				<Card.Header className="bg-secondary text-white">
					{' '}
					All Orders
				</Card.Header>
				{ordersList.map((order) => {
					return (
						<Card.Body key={order._id}>
							<div>
								<h6>Product {order.productId}</h6>
								<p>
									Purchased on {moment(order.purchasedOn).format('MM-DD-YYYY')}
								</p>
								<p>Purchased by {order.userId}</p>
								<h6>
									Total:{' '}
									<span className="text-warning">₱ {order.totalAmount}</span>
								</h6>
								<hr />
							</div>
						</Card.Body>
					);
				})}
			</Card>
		</React.Fragment>
	);
}
