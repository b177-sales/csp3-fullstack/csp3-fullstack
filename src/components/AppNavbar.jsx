import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { Container, Nav, Navbar } from 'react-bootstrap';
import UserContext from '../UserContext';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Badge from '@mui/material/Badge';
import IconButton from '@mui/material/IconButton';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';

export default function AppNavbar() {
	const { user } = useContext(UserContext);

	return (
		<Navbar
			collapseOnSelect
			expand="lg"
			bg="secondary"
			variant="dark"
			className="mb-5"
			class="navbar navbar-expand-lg"
		>
			<Container>
				<Navbar.Brand>OCHIE</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav" className="navBar">
					<Nav className="me-auto d-flex justify-content-end flex-grow-1 pe-5">
						<Nav.Link as={Link} to="/" exact>
							Home
						</Nav.Link>
						<Nav.Link as={Link} to="/products/active" exact>
							Products
						</Nav.Link>
						{(function() {
							if (user.id !== null && user.isAdmin === false) {
								return (
									<React.Fragment>
										<IconButton aria-label="cart">
											<Badge badgeContent={4} color="secondary">
												<ShoppingCartIcon />
											</Badge>
										</IconButton>
										<NavDropdown title="My Account" id="nav-dropdown">
											<Nav.Link
												as={Link}
												to="/userprofile"
												className="text-secondary bg-light"
											>
												Profile
											</Nav.Link>
											<Nav.Link
												as={Link}
												to="/userorderhistory"
												className="text-secondary bg-light"
											>
												Order History
											</Nav.Link>
											<NavDropdown.Divider />
											<Nav.Link
												as={Link}
												to="/logout"
												className="text-secondary bg-light"
											>
												Logout
											</Nav.Link>
										</NavDropdown>
									</React.Fragment>
								);
							} else if (user.id !== null && user.isAdmin === true) {
								return (
									<NavDropdown title="Admin" id="nav-dropdown">
										<Nav.Link
											as={Link}
											to="/adminview"
											className="text-secondary bg-light"
										>
											AdminDashboard
										</Nav.Link>
										<NavDropdown.Divider />
										<Nav.Link
											as={Link}
											to="/logout"
											className="text-secondary bg-light"
										>
											Logout
										</Nav.Link>
									</NavDropdown>
								);
							} else {
								return (
									<React.Fragment>
										<Nav.Link as={Link} to="/login" exact>
											Login
										</Nav.Link>
										<Nav.Link as={Link} to="/register" exact>
											Register
										</Nav.Link>
									</React.Fragment>
								);
							}
						})()}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}
