import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {
	return (
		<Row>
			<Col className="p-5">
				<h1 className="mb-3">WELCOME TO</h1>
				<p className="my-3">Sports Apparrell: We got what you need!</p>
				<Link className="btn btn-dark" to={`/products/active`}>
					GO TO PRODUCTS
				</Link>
			</Col>
		</Row>
	);
}
