import React from 'react';
import { MDBFooter } from 'mdb-react-ui-kit';

export default function Footer() {
	return (
		<MDBFooter
			style={{
				backgroundColor: 'dark',
				color: 'white',
				fontWeight: '500',
				fontFamily: 'Bitter',
				textAlign: 'center',
				marginTop: '75px',
				position: 'sticky',
			}}
		>
			<div
				className="text-center p-2"
				id="footer"
				style={{ backgroundColor: 'grey' }}
			>
				{' '}
				<span>Jose Antonio Sales</span>
				<span> | </span>
				<span>Web Developer</span>
				<span> | </span>
				&copy; {new Date().getFullYear()} All Rights Reserved
			</div>
		</MDBFooter>
	);
}
